/**
 * This file contains validation related functions.
 */
"use strict";

/**
 * Add load event listener to window for form validation.
 */
(function () {
  window.addEventListener(
    "load",
    function () {
      // All forms that we'll apply custom Bootstrap validation styles to.
      var forms = document.getElementsByClassName("validation");
      // Loop over all forms and prevent default form submission.
      var validation = Array.prototype.filter.call(forms, function (form) {
        form.addEventListener(
          "submit",
          function (event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add("was-validated");
          },
          false
        );
      });
    },
    false
  );
})();
